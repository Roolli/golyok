using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Golyok
{
    class BallGameManager
    {
        static Random random = new Random();
        ConsoleColor[,] playArea;
        int score;
        BallGameUIController uIController = new BallGameUIController();
        BallGameDataController dataController = new BallGameDataController();

        /// <summary>
        /// </summary>
        /// <param name="column">Max: 24</param>
        /// <param name="row">Max: 79</param>
        public BallGameManager(int column, int row)
        {
            if (column > 24)
            {
                throw new Exception("A játéktér maximum 24 egység széles lehet!");
            }
            if (row > 79)
            {
                throw new Exception("A játéktér maximum 79 egység magas lehet!");
            }
            GeneratePlayGround(row,column);
            StartGame();
        }
        private void StartGame()
        {
            uIController.AskForPlayerName();
            uIController.StartTimer();
            uIController.DrawPlayArea(in playArea); // in c#7.2 readonly ref  https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/in-parameter-modifier
            while (!EndOfGameCheck())
            {
                int[] coords = uIController.SelectBall();
                RemoveBall(coords[0], coords[1]);
                uIController.DrawPlayArea(in playArea);
            }
            dataController.AddToLeaderboard(uIController.PlayerName, uIController.ElapsedSeconds, score);

        }
        private bool EndOfGameCheck()
        {
            score = 0;
            for (int i = 0; i < playArea.GetLength(0); i++)
            {
                ConsoleColor previous = playArea[i, 0];
                for (int j = 1; j < playArea.GetLength(1); j++)
                {
                    if (ConsoleColor.Black != previous && playArea[i, j] == previous)
                    {
                        return false;
                    }
                    previous = playArea[i, j];
                    if (playArea[i, j] != ConsoleColor.Black)
                    {
                        score++;
                    }
                }
                previous = ConsoleColor.Black;
            }
            return true;
        }

        private void MoveColumns()
        {
            for (int i = playArea.GetLength(1) - 1; i >= 0; i--)
            {
                if (playArea[playArea.GetLength(0) - 1, i] == ConsoleColor.Black)
                {
                    for (int c = i; c < playArea.GetLength(1) - 1; c++)
                    {
                        for (int r = 0; r < playArea.GetLength(0); r++)
                        {
                            playArea[r, c] = playArea[r, c + 1];
                            playArea[r, c + 1] = ConsoleColor.Black;
                        }
                    }
                }
            }
        }

        private void RemoveBall(int x, int y)
        {
            if (BoundaryCheck(x, y - 1) && playArea[x, y - 1] == playArea[x, y])
            {
                playArea[x, y - 1] = ConsoleColor.Black;
                DropBalls(x, y - 1);
            }
            if (BoundaryCheck(x, y + 1) && playArea[x, y + 1] == playArea[x, y])
            {
                playArea[x, y + 1] = ConsoleColor.Black;
                DropBalls(x, y + 1);
            }
            if (BoundaryCheck(x, y))
            {
                playArea[x, y] = ConsoleColor.Black;
                DropBalls(x, y);
            }
            MoveColumns();
        }

        private bool BoundaryCheck(int x, int y)
        {
            return x >= 0 && x <= playArea.GetLength(0) && y >= 0 && y <= playArea.GetLength(1);
        }

        private void DropBalls(int x, int y)
        {
            int i = x;
            while(i >= 1 && playArea[i-1,y] != ConsoleColor.Black)
            {
                ConsoleColor temp = playArea[i, y];
                playArea[i, y] = playArea[i - 1, y];
                playArea[i - 1, y] = temp;
                i--;
            }
        }

        private void GeneratePlayGround(int row,int column)
        {
            playArea = new ConsoleColor[row, column];
            for (int i = 0; i < playArea.GetLength(0); i++)
            {
                for (int j = 0; j < playArea.GetLength(1); j++)
                { 
                        playArea[i, j] = (ConsoleColor)random.Next((int)ConsoleColor.Blue, (int)ConsoleColor.Yellow); // ID 0 (black) Empty space
                }
            }
        }
    }
}
