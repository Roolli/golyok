﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Golyok
{
    class PlayerStatistics
    {
        public PlayerStatistics(string name, int elapsedTime, int score)
        {
            Name = name;
            ElapsedTime = elapsedTime;
            Score = score;
        }

        public string Name { get; private set; }
        public int ElapsedTime { get; private set; }
        public int Score { get; private set; }


    }
}
