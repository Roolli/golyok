﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Golyok
{
    class Golyok
    {
        static void Main(string[] args)
        {
            BallGameManager bgm;
            if (args.Length == 2)
            {
                bgm = new BallGameManager(int.Parse(args[0]), int.Parse(args[1]));
            }
            else
            {
                bgm = new BallGameManager(24, 79);
            }
        }
    }
}

