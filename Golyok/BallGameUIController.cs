﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Golyok
{
    class BallGameUIController
    {
        public int ElapsedSeconds { get; private set; }
        public string PlayerName { get; private set; }
        private Timer t;
        public BallGameUIController()
        {
            Console.Title = $"Mikhel Roland - Golyók játék - Eltelt Idő: 0 mp";
            t = new Timer(1000);
            ElapsedSeconds = 0;
        }

        public void DrawPlayArea(in ConsoleColor[,] playArea)
        {
            Console.Clear();
            for (int i = 0; i < playArea.GetLength(0); i++)
            {
                for (int j = 0; j < playArea.GetLength(1); j++)
                {
                    Console.ForegroundColor = playArea[i, j];
                    Console.Write('O');
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write('|');
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void AskForPlayerName()
        {
            do
            {
                Console.Write("Játékosnév: ");
                PlayerName = Console.ReadLine();
            } while (PlayerName == string.Empty);
        }
        public int[] SelectBall()
        {
            Console.Write("Melyik golyót szeretnéd kiválasztani? (sor oszlop) ");
            string[] temp = Console.ReadLine().Split(' ');
            

            return new int[] { int.Parse(temp[0]) - 1, int.Parse(temp[1]) - 1 }; //Így nem kell -1 et megadni inputkor
        }
        private void StopTimer()
        {
            t.Stop();
            t.Dispose();
        }
        public void StartTimer()
        {
            t.Elapsed += TimerElapsed;
            t.Start();
            
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Console.Title = $"Mikhel Roland - Golyók játék - Eltelt Idő: {++ElapsedSeconds} mp";
        }

    }

}
