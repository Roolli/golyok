﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Golyok
{
    class BallGameDataController
    {
        private PlayerStatistics[] playerList;
        public BallGameDataController()
        {
            LoadData();
        }

        public void AddToLeaderboard(string playerName,int elapsedSeconds,int score)
        {

            playerList[playerList.Length - 1] = new PlayerStatistics(playerName, elapsedSeconds, score);
            SaveLeaderboard();
        }

        private void LoadData()
        {
            if (File.Exists("leaderboard.txt"))
            {
               string[] fileContents = File.ReadAllLines("leaderboard.txt");
                playerList = new PlayerStatistics[fileContents.Length+1];
                for (int i = 0; i < fileContents.Length; i++)
                {
                    string[] line = fileContents[i].Split('|');
                    playerList[i] = new PlayerStatistics(line[0], int.Parse(line[1]), int.Parse(line[2]));
                }
            }
            else
            {
                playerList = new PlayerStatistics[1];
            }
        }
        private void SaveLeaderboard()
        {
            SortLeaderboard();
            using (StreamWriter sw = new StreamWriter("leaderboard.txt"))
            {
                for (int i = 0; i < playerList.Length; i++)
                {
                    sw.WriteLine($"{playerList[i].ElapsedTime}|{playerList[i].Score}|{playerList[i].Name}");
                }
            }
        }

        private void SortLeaderboard()
        {
            //Sort by elapsed time in descending order
            for (int i = 0; i < playerList.Length-1; i++)
            {
                for (int j = i; j < playerList.Length; j++)
                {
                    if(playerList[i].ElapsedTime > playerList[j].ElapsedTime)
                    {
                        Swap(playerList[i], playerList[j]);
                       
                    } 
                }
            }
            //Sort by score in ascending order if 2 times are equal
            for (int i = 1; i < playerList.Length-1; i++)
            {
                if(playerList[i-1].ElapsedTime == playerList[i].ElapsedTime && playerList[i-1].Score > playerList[i].Score)
                {
                    Swap(playerList[i - 1], playerList[i]);
                }
            }
        }

        private void Swap(PlayerStatistics playerStatistics1, PlayerStatistics playerStatistics2)
        {
            PlayerStatistics tempPlayer = playerStatistics1;
            playerStatistics1 = playerStatistics2;
            playerStatistics2 = tempPlayer;
        }
    }
}
